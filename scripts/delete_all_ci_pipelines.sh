#!/bin/bash

# Check if required tools are installed
if ! command -v curl &>/dev/null; then
    echo "Error: curl is not installed."
    exit 1
fi

if ! command -v jq &>/dev/null; then
    echo "Error: jq is not installed. Install it to parse JSON responses."
    exit 1
fi

# Check if environment variables are set
if [ -z "$PROJECT_ID" ]; then
    echo "Error: PROJECT_ID environment variable is not set."
    exit 1
fi

if [ -z "$ACCESS_TOKEN" ]; then
    echo "Error: ACCESS_TOKEN environment variable is not set."
    exit 1
fi

# Set GitLab API URL
GITLAB_API_URL="https://gitlab.com/api/v4"
SLEEP_INTERVAL=60 # Adjust this value to control the delay between API requests

# Function to delete a pipeline
delete_pipeline() {
    local pipeline_id=$1
    response=$(curl -s -X DELETE -H "Authorization: Bearer $ACCESS_TOKEN" "$GITLAB_API_URL/projects/$PROJECT_ID/pipelines/$pipeline_id")
    if [ -z "$response" ]; then
        echo "Pipeline $pipeline_id deleted successfully."
    else
        echo "Failed to delete pipeline $pipeline_id: $response"
    fi
    sleep $SLEEP_INTERVAL # Delay to reduce API load
}

# Fetch and delete pipelines with pagination
page=1
per_page=100 # Maximum allowed by GitLab API
while true; do
    echo "Fetching pipelines (page $page)..."
    pipelines=$(curl -s -H "Authorization: Bearer $ACCESS_TOKEN" \
        "$GITLAB_API_URL/projects/$PROJECT_ID/pipelines?page=$page&per_page=$per_page")

    # Check if there are pipelines
    pipeline_count=$(echo "$pipelines" | jq '. | length')
    if [ "$pipeline_count" -eq 0 ]; then
        echo "No more pipelines to delete."
        break
    fi

    # Delete pipelines in parallel
    echo "Deleting $pipeline_count pipelines..."
    echo "$pipelines" | jq -r '.[].id' | while read -r pipeline_id; do
        delete_pipeline "$pipeline_id" &
    done

    # Wait for all background processes to complete
    wait

    # Increment page
    page=$((page + 1))
done

echo "All pipelines deleted."
